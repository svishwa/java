// Name: Sandesh Vishwanath
// USC NetID: sandeshv
// CS 455 PA4
// Fall 2017
import java.io.FileNotFoundException;
import java.util.*;

/**
 * Finds scores of all possible subsets of a given string and sorts them
 * according to their score
 * 
 */

public class WordFinder {
	/**
	 * finds the score of each subset of string by comparing it to the given dictionary file
	 * @param args represents the file name
	 */
	public static void main(String[] args) {
		ArrayList<String> ar = new ArrayList<String>();
		ArrayList<String> listWords;
		AnagramDictionary ad;
		try {
			if (args.length != 0)// checks if the dictionary file is given as
									// part of command line arguments
				ad = new AnagramDictionary(args[0]);
			else
				// if file not give sopods.txt will be taken by default
				ad = new AnagramDictionary("swopods.txt");
			Scanner in = new Scanner(System.in);
			System.out.println("Type . to quit.");
			while (true) {
				System.out.println("Rack?");
				String lineWord = in.next();// reads in next word
				listWords = new ArrayList<String>();
				if (!lineWord.equals(".")) {
					Rack rk = new Rack(lineWord);
					ArrayList<String> wordList = new ArrayList<String>();
					wordList = rk.findSubsets(rk.getString(), rk.getFrequency());// contains
																			// arraylist
																			// of
																			// string
																			// containing
																			// all
																			// subsets
																			// of
																			// a
																			// given
																			// string
					for (String s : wordList) {
						ar = ad.getAnagramsOf(s);// gets array list of string
													// anagrams of a given
													// string
						if (!ar.isEmpty())
							listWords.addAll(ar);// adds all anagrams of all
													// subset
					}
					char[] sortedLineWord = lineWord.toCharArray();
					Arrays.sort(sortedLineWord);// to print out sorted version
												// of given string
					System.out.println("We can make " + listWords.size()
							+ " words from " + new String(sortedLineWord));
					// System.out.println(listWords);
					ScoreTable sct = new ScoreTable();
					listWords = sct.getScoreAll(listWords);// gets sorted
															// arrayList
															// according to
															// their scores
					for (String s : listWords) {
						System.out.println(sct.getScore(s) + ": " + s);
					}
				} 
				else// if the entered input is . stop searching for score
				{
					break;
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println(args[0] + " file not found");// prints out file
															// not found if file
															// name doesn't
															// match
		}

	}
}
