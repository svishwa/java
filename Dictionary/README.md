##The assignment##
You will be implementing a program that when given letters that could comprise a Scrabble rack, creates a list of all legal words that can be formed from the letters on that rack. To solve the problem you will also need a scrabble dictionary (we'll provide that for you). Some particulars of the Scrabble dictionary: it only has words of length two or more, and it includes all forms of a word as separate entries, e.g., singular plus plural, verb conjugations.
For example, if your rack had the letters C M A L you could rearrange the letters to form the words calm or clam, but you could also form shorter words from a subset of the letters, e.g., lam or ma. It's generally difficult to figure out all such sequences of the letters that form real words (unless you are a tournament Scrabble competitor who knows the Scrabble dictionary very well).

For your program, you will display all such words, with the corresponding Scrabble score for each word, in decreasing order by score. For words with the same scrabble score, the words must appear in alphabetical order. Here are the results for a rack consisting of "cmal" (using the sowpods dictionary) in the output format you will be using for your program (user input is shown in italics):

Rack? cmal
We can make 11 words from "aclm"
All of the words with their scores (sorted by score):
8: calm
8: clam
7: cam
7: mac
5: lac
5: lam
5: mal
4: am
4: ma
2: al
2: la
Note that "aclm" above is just a version of the rack with the letters rearranged in alphabetical order.
We'll provide you the Scrabble score for each letter later in this document .

Here's more about exactly how to run your program and what happens:

Your program will take an optional command-line argument for the dictionary file name. If that argument is left off, it will use the Scrabble dictionary file sowpods.txt (see assignment files) from the same directory as you are running your program. If the dictionary file specified (either explicitly or the default one) does not exist, your program will print an informative error message (that includes the file name) and exit.

The initial program output will be the message:

Type . to quit.
Then the program will run in a loop on the console, printing the prompt "Rack? " (as seen in the earlier example) and reading and processing each rack you enter, until you tell it to exit. You tell the program to exit by typing in "." at the prompt (i.e., a period). We aren't use a command such as "quit" as the sentinel, since that could be a legal rack.
We provided you a few sample data files, and corresponding correct reference output from running those on the sowpods.txt (the Scrabble dictionary given) in the testFiles directory. Your output must match the reference output character by character.

The real game of Scrabble has only upper-case letters on tiles, but for our program we'll accept any sequence of non-whitespace characters as a legal "rack." However, words will only be able to be formed from actual letters if that's what's in the given dictionary. E.g., if the rack given is "abc@" you will report the words such as "cab", but there will be no words containing "@", since @ doesn't appear in any dictionary words.

The program will work on both lower-and-upper case versions of dictionaries, but all processing will be case-sensitive. E.g., if the dictionary given has only upper-case versions of words, it will find words from a rack such as "CMAL", but won't be able to find any words from the rack "cmal".

