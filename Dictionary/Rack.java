// Name: Sandesh Vishwanath
// USC NetID: sandeshv
// CS 455 PA4
// Fall 2017

import java.util.ArrayList;
import java.util.*;

/**
 * A Rack of Scrabble tiles
 */

public class Rack {
	private String str;
	private int[] freq;

	/**
	 * creates rack of all possible subset for a given string
	 * 
	 * @param word
	 */
	public Rack(String word) {
		String s = word;
		str = "";
		freq = new int[s.length()];
		Map<Character, Integer> map = new HashMap<Character, Integer>();
		for (int i = 0; i < s.length(); i++) {// to calculate appearance of each
												// character and its frequency
												// in an HashMap
			char ch = s.charAt(i);
			Integer val = map.get(new Character(ch));
			if (val != null)// if character already present increase its value
							// in map
			{
				map.put(ch, val + 1);
			} else// initialize to one for first occurrence of a character
			{
				map.put(ch, 1);
			}
		}
		int i = 0;// index for freq int array
		for (Map.Entry<Character, Integer> entry : map.entrySet()) {

			freq[i] = entry.getValue();// puts frequency of each word in an int
										// array
			i++;
			this.str += entry.getKey();// puts unique words in a string
		}

	}

	/**
	 * getter function
	 * 
	 * @return unique string
	 */
	public String getString() {
		return str;
	}

	/**
	 * getter function
	 * 
	 * @return frequency of each character in a unique string
	 */
	public int[] getFrequency() {
		return freq;
	}

	/**
	 * Finds all subsets of the multiset starting at position k in unique and
	 * mult. unique and mult describe a multiset such that mult[i] is the
	 * multiplicity of the char unique.charAt(i). PRE: mult.length must be at
	 * least as big as unique.length() 0 <= k <= unique.length()
	 * 
	 * @param unique
	 *            a string of unique letters
	 * @param mult
	 *            the multiplicity of each letter from unique.
	 * @param k
	 *            the smallest index of unique and mult to consider.
	 * @return all subsets of the indicated multiset
	 * @author Claire Bono
	 */
	private static ArrayList<String> allSubsets(String unique, int[] mult, int k) {
		ArrayList<String> allCombos = new ArrayList<String>();

		if (k == unique.length()) { // multiset is empty
			allCombos.add("");
			return allCombos;
		}

		// get all subsets of the multiset without the first unique char
		ArrayList<String> restCombos = allSubsets(unique, mult, k + 1);

		// prepend all possible numbers of the first char (i.e., the one at
		// position k)
		// to the front of each string in restCombos. Suppose that char is
		// 'a'...

		String firstPart = ""; // in outer loop firstPart takes on the values:
								// "", "a", "aa", ...
		for (int n = 0; n <= mult[k]; n++) {
			for (int i = 0; i < restCombos.size(); i++) { // for each of the
															// subsets
															// we found in the
															// recursive call
				// create and add a new string with n 'a's in front of that
				// subset
				allCombos.add(firstPart + restCombos.get(i));
			}
			firstPart += unique.charAt(k); // append another instance of 'a' to
											// the first part
		}

		return allCombos;
	}

	/**
	 * helper function to start with index of a string so as to get all the sets
	 * 
	 * @param word
	 *            a string of unique letters
	 * @param multi
	 *            multiplicity of each letter from unique.
	 * @return all subsets of the indicated multiset
	 */
	public ArrayList<String> findSubsets(String word, int[] multi) {
		return allSubsets(word, multi, 0);
	}

}