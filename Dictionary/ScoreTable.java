// Name: Sandesh Vishwanath
// USC NetID: sandeshv
// CS 455 PA4
// Fall 2017
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * Creates a class containing score for each character
 * 
 */
public class ScoreTable {
	private int[] arr = new int[26];
	private Map<String, Integer> map;

	/**
	 * Initalizes all character to be represented by index and are associated
	 * with specific values
	 */
	public ScoreTable() {
		map = new HashMap<String, Integer>();
		arr[0] = 1;
		arr[1] = 3;
		arr[2] = 3;
		arr[3] = 2;
		arr[4] = 1;
		arr[5] = 4;
		arr[6] = 2;
		arr[7] = 4;
		arr[8] = 1;
		arr[9] = 8;
		arr[10] = 5;
		arr[11] = 1;
		arr[12] = 3;
		arr[13] = 1;
		arr[14] = 1;
		arr[15] = 3;
		arr[16] = 10;
		arr[17] = 1;
		arr[18] = 1;
		arr[19] = 1;
		arr[20] = 1;
		arr[21] = 4;
		arr[22] = 4;
		arr[23] = 8;
		arr[24] = 4;
		arr[25] = 10;
	}

	/**
	 * finds score of a given string by combining score of all characters
	 * 
	 * @param string
	 *            score to be calculated
	 * @return score of a given string
	 */
	public int getScore(String string) {
		if (map.containsKey(string))// to provide faster score as getScore
									// function is called again in main function
			return map.get(string);

		else {

			char[] stringChar = string.toCharArray();
			int score = 0;
			for (int i = 0; i < stringChar.length; i++)// calculates score of a
														// string
			{
				score = score + arr[Character.toLowerCase(stringChar[i]) - 'a'];// will
																				// work
																				// for
																				// both
																				// upper
																				// case
																				// and
																				// lower
																				// case
																				// characters

			}

			return score;
		}
	}

	/**
	 * finds sorted list of strings
	 * 
	 * @param listWords
	 *            contains all string subsets of a string
	 * @return sorted list of strings
	 */
	public ArrayList<String> getScoreAll(ArrayList<String> listWords) {
		Collections.sort(listWords, new Comparator<String>() {

			public int compare(String arg0, String arg1) {
				int score1 = getScore(arg1);
				int score0 = getScore(arg0);
				map.put(arg0, score0);
				map.put(arg1, score1);
				if (score1 == score0)// if scores are equal sort by string
					return arg0.compareTo(arg1);
				else
					// sort by score of that string
					return score1 - score0;
			}
		});
		return listWords;
	}
}
