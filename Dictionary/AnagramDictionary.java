// Name: Sandesh Vishwanath
// USC NetID: sandeshv
// CS 455 PA4
// Fall 2017

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.io.File;
import java.util.Scanner;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * A dictionary of all anagram sets. Note: the processing is case-sensitive; so
 * if the dictionary has all lower case words, you will likely want any string
 * you test to have all lower case letters too, and likewise if the dictionary
 * words are all upper case.
 */

public class AnagramDictionary {
	private Map<String, String> anagramDict;

	/**
	 * Create an anagram dictionary from the list of words given in the file
	 * indicated by fileName. PRE: The strings in the file are unique.
	 * 
	 * @param fileName
	 *            the name of the file to read from
	 * @throws FileNotFoundException
	 *             if the file is not found
	 */
	public AnagramDictionary(String fileName) throws FileNotFoundException {
		Scanner name = new Scanner(new File(fileName));
		anagramDict = new HashMap<String, String>();
		while (name.hasNext())// sorts all the strings available in the
								// dictionary and stores it in an hashmap
		{
			String word = name.next();
			char[] wordChar = word.toCharArray();
			Arrays.sort(wordChar);
			anagramDict.put(word, new String(wordChar));
		}

	}

	/**
	 * Get all anagrams of the given string. This method is case-sensitive. E.g.
	 * "CARE" and "race" would not be recognized as anagrams.
	 * 
	 * @param s
	 *            string to process
	 * @return a list of the anagrams of s
	 * 
	 */
	public ArrayList<String> getAnagramsOf(String s) {
		ArrayList<String> array = new ArrayList<String>();
		for (Map.Entry<String, String> entry : anagramDict.entrySet()) {
			// System.out.println(entry.getKey()+" "+entry.getValue());
			char[] sChar = s.toCharArray();
			Arrays.sort(sChar);// sorts the given string
			s = new String(sChar);
			if (s.equals(entry.getValue()))// sorted string is compared to
											// hashmap values if same,it is the
											// anagram of the given string and
											// all possible keys are added to
											// string array
			{
				array.add(entry.getKey());
			}
		}
		return array; // DUMMY CODE TO GET IT TO COMPILE
	}

}