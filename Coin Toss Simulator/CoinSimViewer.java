import java.util.Scanner;
import javax.swing.JFrame;
/** class CoinSimViewer
 *accepts the number of trials to be carried out
 */
public class CoinSimViewer
{
    /** CoinTossSimulator object is created and its methods are called
	@param args not used*/
    public static void main(String[] args){
	int num;
	while(true){
	Scanner input=new Scanner(System.in);
	System.out.print("Enter number of trials :");
	num=input.nextInt();
	if(num<1)
	    {System.out.print("ERROR: Number entered must be greater than 0.");
	    continue;}
	else{
		JFrame frame=new JFrame();
		frame.setSize(800,500);
		frame.setTitle("CoinSim");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		CoinSimComponent component=new CoinSimComponent(num);
		frame.add(component);
		frame.setVisible(true);
		continue;}
	}
	

    }}
