import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JComponent;
/** class CoinSimComponent 
 * 
 * Displays the bars in frame
 *
 */
public class CoinSimComponent extends JComponent
{
	private int head,tail,headTail;
    private int numOfTrials;
    double h_p,t_p,ht_p;//to store head,tail and head and a tail percentage
    
    /**
     * With number of trials obtained, it is passed to CoinTossSimulator object constructor
     * @param trials indicates number of times the coin has to be tossed
     */
    public CoinSimComponent(int trials)
    {
	numOfTrials=trials;
	CoinTossSimulator toss=new CoinTossSimulator();
	toss.run(numOfTrials);
	head=toss.getTwoHeads();
	tail=toss.getTwoTails();
	headTail=toss.getHeadTails();
	h_p= (double)head/trials;
	t_p=(double)tail/trials;
	ht_p=(double)headTail/trials;
	h_p=h_p*100;
	t_p=t_p*100;
	ht_p=ht_p*100;
	
    }
   
    /**
     * Three bars are created
     * @param g graphics context
     */
    public void paintComponent(Graphics g)
    {	Graphics2D g2=(Graphics2D) g;
         int LeftLocation=100;
         int bottomloc=getHeight();
         int width=100;
         double scale=0;
         //to set the scale for vertical scaling
         if(numOfTrials<bottomloc)
          scale =(double)bottomloc/(numOfTrials+2);
         else{
        	 scale = (double)bottomloc/numOfTrials;
        	 }
	//Creates bar for toss with head as result
	Bar bar1=new Bar(bottomloc,LeftLocation,width,head,scale,Color.blue,"Two Heads: "+head+" ("+(int)h_p+"%)");
	bar1.draw(g2);
	
	//Creates bar for toss with tail as result
	Bar bar2=new Bar(bottomloc,LeftLocation+width+LeftLocation,width,tail,scale,Color.GREEN,"Two Tails: "+tail+" ("+(int)t_p+"%)");
	bar2.draw(g2);
	
	//Creates bar for toss with head and tail as result
	Bar bar3=new Bar(bottomloc,LeftLocation+width+LeftLocation+width+LeftLocation,width,headTail,scale,Color.YELLOW,"A Head and a Tail: "+headTail+" ("+(int)ht_p+"%)");
	bar3.draw(g2);
	
	
    }
}
