import java.util.Random;
// Name:Sandesh Vishwanath
// USC NetID: 2100554211
// CS 455 PA1
// Fall 2017

/**
 * class CoinTossSimulator
 *  Simulates trials of tossing two coins by taking the number of trials input from user
 * 
 */
public class CoinTossSimulator {
    private int headCount,tailCount,headTailCount;
    private Random generator;

   /**
      Creates a coin toss simulator with no trials done yet.
   */
   public CoinTossSimulator() {
       headCount=0;
       tailCount=0;
       headTailCount=0;
       generator=new Random();
   }


   /**
      Runs the simulation for numTrials more trials. Multiple calls to this method
      without a reset() between them *add* these trials to the current simulation.
      
      @param numTrials  number of trials to for simulation; must be >= 1
    */
   public void run(int numTrials) {
	   int i;
       for(i=1;i<=numTrials;i++)
	   {
	       int res1=generator.nextInt(2);//holds result of first toss 
	       int res2=generator.nextInt(2);//holds result of second toss
	     
       if(res1==1 && res2==1)
	   {
	       headCount++;}
       else if(res1==0 && res2==0)// 1 represents head and 0 represents tail
       {
    	   tailCount++;
       }
       else{
    	   headTailCount++;
       }
       if(i==1000000000)
       {
    	   System.out.println(i);
       }
       else if(i==2000000000)
       {
    	   System.out.println(i);
       }
       if(i==2147483647)
       {
    	   System.out.println(i);
    	   break;
       }
	   }
       
   }


   /**
      Get number of trials performed since last reset.
      @return gets the total number of trials
   */
   public int getNumTrials() {
       return getTwoHeads() + getTwoTails() + getHeadTails();
   }


   /**
      Get number of trials that came up two heads since last reset.
      @return gets the 2 heads count
   */
   public int getTwoHeads() {
       return headCount; 
   }


   /**
     Get number of trials that came up two tails since last reset.
     @return gets the 2 tails count
   */  
   public int getTwoTails() {
       return tailCount; 
   }


   /**
     Get number of trials that came up one head and one tail since last reset.
     @return gets the 1 head and 1 tail count
   */
   public int getHeadTails() {
       return headTailCount;    }


   /**
      Resets the simulation, so that subsequent runs start from 0 trials done.
    */
   public void reset() {
       headCount=tailCount=headTailCount=0;

   }
}
