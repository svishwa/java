
// Name:Sandesh Vishwanath
// USC NetID: 2100554211
// CS 455 PA1
// Fall 2017

/**
 * class CoinTossSimulatorTester
 * 
 * Simulates trials of tossing two coins and allows the user to access the
 * cumulative results.
 * 
 * NOTE: we have provided the public interface for this class.  Do not change
 * the public interface.  You can add private instance variables, constants, 
 * and private methods to the class.  You will also be completing the 
 * implementation of the methods given. 
 * 
 * Invariant: getNumTrials() = getTwoHeads() + getTwoTails() + getHeadTails()
 * 
 */
public class CoinTossSimulatorTester {

    public static void main(String[] args)
    {
	CoinTossSimulator test=new CoinTossSimulator();
	int num=0;
	System.out.println("After constructor:");
	System.out.print("Number of trials[exp:0] :"+test.getNumTrials());
	System.out.print("\nTwo head tosses :"+test.getTwoHeads());
	System.out.print("\nTwo tail tosses :"+test.getTwoTails());
	System.out.print("\nOne-head one-tail tosses :"+test.getHeadTails());
	boolean check=(num==test.getNumTrials())?true:false;
	System.out.print("\nTosses add up correctly?"+check);
	System.out.println("\n");

	int num1=1;
	test.run(num1);
	System.out.println("After run(1):");
	System.out.print("Number of trials[exp:1] :"+test.getNumTrials());
	System.out.print("\nTwo head tosses :"+test.getTwoHeads());
	System.out.print("\nTwo tail tosses :"+test.getTwoTails());
	System.out.print("\nOne-head one-tail tosses :"+test.getHeadTails());
	check=(num1==test.getNumTrials())?true:false;//checks whether given number of trials is same total number of trials
	System.out.print("\nTosses add up correctly?"+check);
	System.out.println("\n");

	int num2=10;
	test.run(num2);
	System.out.println("After run(10):");
	System.out.print("Number of trials[exp:11] :"+test.getNumTrials());
	System.out.print("\nTwo head tosses :"+test.getTwoHeads());
	System.out.print("\nTwo tail tosses :"+test.getTwoTails());
	System.out.print("\nOne-head one-tail tosses :"+test.getHeadTails());
	check=((num1+num2)==test.getNumTrials())?true:false;
	System.out.print("\nTosses add up correctly?"+check);
	System.out.println("\n");

	
	System.out.println("After reset: ");
	test.reset();//resets the object's instance variable
    num=0;
	System.out.print("Number of trials[exp:0] :"+test.getNumTrials());
	System.out.print("\nTwo head tosses :"+test.getTwoHeads());
	System.out.print("\nTwo tail tosses :"+test.getTwoTails());
	System.out.print("\nOne-head one-tail tosses :"+test.getHeadTails());
	check=(num==test.getNumTrials())?true:false;
	System.out.print("\nTosses add up correctly?"+check);
	System.out.println("\n");

	int num3=2147483647;
	test.run(num3);
	System.out.println("After run(100):");
	System.out.print("Number of trials[exp:100] :"+test.getNumTrials());
	System.out.print("\nTwo head tosses :"+test.getTwoHeads());
	System.out.print("\nTwo tail tosses :"+test.getTwoTails());
	System.out.print("\nOne-head one-tail tosses :"+test.getHeadTails());
	check=(num3==test.getNumTrials())?true:false;
	System.out.print("\nTosses add up correctly?"+check);
	System.out.println("\n");

    }}
