// Name:Sandesh Vishwanath
// USC NetID:2100554211
// CS 455 PA1
// Fall 2017

// we included the import statements for you
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;

/**
 * Bar class
 * A labeled bar that can serve as a single bar in a bar graph.
 * The text for the label is centered under the bar.
 * 
 * NOTE: we have provided the public interface for this class. Do not change
 * the public interface. You can add private instance variables, constants,
 * and private methods to the class. You will also be completing the
 * implementation of the methods given.
 * 
 */
public class Bar {
    private int bottomLocation;
    private int leftLocation;
    private int barWidth;
    private int height;
    private double numberOfPixels;
    private Color barColor;
    private String barName;
    private static final int CONST=40;
   /**
      Creates a labeled bar.  You give the height of the bar in application
      units (e.g., population of a particular state), and then a scale for how
      tall to display it on the screen (parameter scale). 
  
      @param bottom  location of the bottom of the label
      @param left  location of the left side of the bar
      @param width  width of the bar (in pixels)
      @param barHeight  height of the bar in application units
      @param scale  how many pixels per application unit
      @param color  the color of the bar
      @param label  the label at the bottom of the bar
   */
   public Bar(int bottom, int left,int width, int barHeight,
              double scale, Color color, String label) {
       bottomLocation=bottom;
       leftLocation=left;
       barWidth=width;
       height=barHeight;
       numberOfPixels=scale;
       barColor=color;
       barName=label;
   }
   
   /**
      Draw the labeled bar. 
      @param g2  the graphics context
   */
       public void draw(Graphics2D g2) {
       double heightnew=(double)(height*numberOfPixels);
       
        
       g2.setColor(barColor);
       Rectangle rect=new Rectangle(leftLocation,bottomLocation-CONST,barWidth,(int)(heightnew));
       rect.translate(0,(int)(-heightnew));//to reverse the position of the bars created as by default bars will be created in 4th quadrant
      
       g2.fill(rect);
       g2.setColor(Color.GRAY);
       g2.drawString(barName,leftLocation,bottomLocation-CONST/2);
      
       
   }
}
