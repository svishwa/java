// Name:Sandesh Vishwanath
// USC NetID:sandeshv
// CSCI 455 PA5
// Fall 2017

// Table.cpp  Table class implementation


/*
 * Modified 11/22/11 by CMB
 *   changed name of constructor formal parameter to match .h file
 */

#include "Table.h"

#include <iostream>
#include <string>
#include <cassert>


// listFuncs.h has the definition of Node and its methods.  -- when
// you complete it it will also have the function prototypes for your
// list functions.  With this #include, you can use Node type (and
// Node*, and ListType), and call those list functions from inside
// your Table methods, below.

#include "listFuncs.h"


//*************************************************************************

/**
 *default constructor
 */
Table::Table() {
  hashSize=HASH_SIZE;
  table=new ListType[hashSize];
  for(int i=0;i<hashSize;i++)
    table[i]=NULL;//all entries will be initialized to NULL
}

/**
 *Constructor for given bucket size
 *@param hSize hash table size
 */ 
Table::Table(unsigned int hSize) {
  hashSize=hSize;
  table=new ListType[hashSize];
  for(int i=0;i<hashSize;i++)
    table[i]=NULL;
}

/**
 *Finds address of value of key asked to be searched
 *@param key to be searched
 *@return adress of value found else NULL
 */ 
int * Table::lookup(const string &key) {
  unsigned int index=hashCode(key);
  ListType node=table[index];
  return doSearch(node,key);
}

/**
 *removes key from the bucket
 *@param key to be removed
 *@return true if successful else false
 */
bool Table::remove(const string &key) {
  unsigned int index=hashCode(key);
  int res=delNode(table[index],key);
  if(res==0)
    return false;  
  return true;
}

/**
 *to add new node for given key and value
 *@param key key to be added
 *@param val to be added in a node
 *@return true if successful else false
 */
bool Table::insert(const string &key, int value) {
  unsigned int index=hashCode(key);
  int res=addNode(table[index],key,value);
  if(res==0)
    return false; 
  return true;
}

/**
 *counts number of nodes in each linkedlist bucket
 *@return number of enteries in table
 */ 
int Table::numEntries() const {
  int entries=0;
  ListType node;
  int i=0;
  while(i<hashSize)
    {
      node=table[i];
      entries+=depth(node);
      i++;
    }
  return entries;      
}

/**
 *prints all buckets with each node in same linked list printed out seperated by tab and rest by new line
 */
void Table::printAll() const {
  ListType node;
  int i=0;
  while(i<hashSize)
    {
      node=table[i];
      printNodes(node);//calls linkedlist functions
      i++;
    }

}


void Table::hashStats(ostream &out) const {
  int maxi=0,nonEmptyEntries=0;
  ListType node;
  for(int i=0;i<hashSize;i++)
    {
      node=table[i];
      nonEmptyEntries+=checkNullNode(node);//counts non empty buckets
      int size=depth(node);
      if(size>maxi)
	maxi=size;
    }
  out<<"Number of buckets : "<<hashSize<<"\n";
  out<<"Number of entries : "<<numEntries()<<endl;
  out<<"Number of non-emptry buckets : "<<nonEmptyEntries<<endl;
  out<<"Longest chain : "<<maxi<<endl; 
  
}


// add definitions for your private methods here
