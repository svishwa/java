// Name:Sandesh Vishwanath
// USC NetID:sandeshv
// CSCI 455 PA5
// Fall 2017


#include <iostream>

#include <cassert>

#include "listFuncs.h"

using namespace std;

Node::Node(const string &theKey, int theValue) {
  key = theKey;
  value = theValue;
  next = NULL;
}

Node::Node(const string &theKey, int theValue, Node *n) {
  key = theKey;
  value = theValue;
  next = n;
}




//*************************************************************************
// put the function definitions for your list functions below

/**
 *checks if a given key is present in the buckets
 *@param list bucket passed for checking
 *@param key to be searched
 *@return  returns address of value if key is present else NULL
 */
int* doSearch(ListType list,string key)
{
  ListType tmp=list;
  if(tmp!=NULL)
    {
      while(tmp!=NULL)
	{
	  if(tmp->key==key)
	    {
	      return &(tmp->value);//address of value of the key searched
	    }
	  tmp=tmp->next;//moves to next node 
	}
  }
  return NULL;
}

/**
 *checks length of a linked list for a particular bucket
 *@param list bucket passed for checking
 *@return returns size of linked list
 */
int depth(ListType list)
{
  int size=0;
  if(list!=NULL)
    {
      while(list!=NULL)
	{
	  size++;//updates count for each node
	  list=list->next;
	}
    }
  return size;
}

/** 
 *prints all nodes for a given bucket
 *@param list bucket passed for checking
 */
void printNodes(ListType list)
{
  ListType tmp=list;
  if(tmp!=NULL)
    {
      while(tmp!=NULL)
	{
	  cout<<tmp->key<<"->"<<tmp->value<<"\t";
	  tmp=tmp->next;
	}
      cout<<endl;
    }
  else
    return;
}
/**
 *checks for null node ie empty bucket
*@param list is the bucket to be checked
*@return 1 if node is not empty
*/
int checkNullNode(ListType list)
{
  if(list!=NULL)
    return 1;
  return 0;
}

/**
 *inserts new node to the right side for given bucket
 *@param list bucket passed for checking
 *@param key to be saved
 *@param val to be saved in a node
 *@return 1 if successful else 0
 */
int addNode(ListType &list,string key,int val)
{
  ListType &tmp=list;
  ListType head=list;//saves address head of linked list
  if(tmp==NULL)
  {
    tmp=new Node(key,val);
    return 1;
  }
  while(tmp->next!=NULL)
  {
    tmp=tmp->next;
  }
  if(tmp->key==key)//if key is already present then value will not be added
      return 0;
  tmp->next=new Node(key,val);//attached new node to the right side
  tmp=head;//address of head node is assigned back
  return 1;
}

/**
 *deletes a given node
 *@param list bucket passed for checking
 *@param key node to be deleted
 *return 1 if successful else 0
 */
int delNode(ListType &list,string key)
{
  ListType tmp;//to store node to be deleted
  ListType head=list;//to refer head of list
  ListType prev=NULL;//to store previous node of a node to be deleted
  if(list==NULL)
    return 0;
  if(list->key==key)//if first node has to be deleted
    {
      tmp=list;
      list=list->next;
      delete tmp;//frees up the memory
      return 1;
    }
  while(list!=NULL && list->key!=key)
    {
      prev=list;//updates prev 
      list=list->next;
    }
  if(list->key==key)//if node to be deleted is found
  {
    tmp=list;
    prev->next=list->next;//previous node of node to be deleted is made to point to the next node of node to be deleted
    delete tmp;
    list=head;//to point to head of the list so that when the fucntion is called again it will be from head or else last updates list position
    return 1;
  }
  return 0;
}
