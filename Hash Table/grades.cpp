// Name:Sandesh Vishwanath
// USC NetID:sandeshv
// CSCI 455 PA5
// Fall 2017

/*
 * grades.cpp
 * A program to test the Table class.
 * How to run it:
 *      grades [hashSize]
 * 
 * the optional argument hashSize is the size of hash table to use.
 * if it's not given, the program uses default size (Table::HASH_SIZE)
 *
 */

#include "Table.h"

// cstdlib needed for call to atoi
#include <cstdlib>

int main(int argc, char * argv[]) {

  // gets the hash table size from the command line

  int hashSize = Table::HASH_SIZE;

  Table * grades;  // Table is dynamically allocated below, so we can call
                   // different constructors depending on input from the user.

  if (argc > 1) {
    hashSize = atoi(argv[1]);  // atoi converts c-string to int

    if (hashSize < 1) {
      cout << "Command line argument (hashSize) must be a positive number" 
	   << endl;
      return 1;
    }

    grades = new Table(hashSize);

  }
  else {   // no command line args given -- use default table size
    grades = new Table();
  }


  grades->hashStats(cout);

  // add more code here
  // Reminder: use -> when calling Table methods, since grades is type Table*

  
  string summary="insert name score\n\tInsert this name and score in the grade table. If this name was already present, print a message to that effect, and don't do the insert.\nchange name newscore\n\tChange the score for name. Print an appropriate message if this name isn't present.\nlookup name\n\tLookup the name, and print out his or her score, or a message indicating that student is not in the table.\nremove name\n\tRemove this student. If this student wasn't in the grade table, print a message to that effect.\nprint\n\tPrints out all names and scores in the table.\nsize\n\tPrints out the number of entries in the table.\nstats\n\tPrints out statistics about the hash table at this point. (Calls hashStats() method)\nhelp\n\tPrints out a brief command summary.\nquit\n\tExits the program.\n";//for brief summary
  string choice;//to store users command
  while(true)
    {
      cout<<"cmd>";
      cin>>choice;
      if(choice=="insert")
	{
	  string name;
	  int score;
	  cin>>name>>score;
	  bool res=grades->insert(name,score);
	  if(res==true)
	    cout<<"Successful"<<endl;
	  else
	    cout<<"This name already exists, so the grades will not be entered"<<endl;
	}
      else if(choice=="change")
	{
	  string name;
	  int newscore;
	  cin>>name>>newscore;
	  int * res=grades->lookup(name);//lookup is used for both updating value and also to search
	  if(res==NULL)
	    cout<<"Given name is not present"<<endl;
	  else
	   {
	     *res=newscore;
	     cout<<"Updated with the new score"<<endl;
	   }
      
	}
      else if(choice=="lookup")
	{
	  string name;
	  cin>>name;
	  int * res=grades->lookup(name);
	  if(res==NULL)
	    cout<<"Lookup failed!! Given name is not present"<<endl;
	  else
	    cout<<"Score of "<<name<<": "<<*res<<endl;
	}
      else if(choice=="remove")
	{
	  string name;
	  cin>>name;
	  bool res=grades->remove(name);
	  if(res==true)
	    cout<<"Given name was removed successfully"<<endl;
	  else
	    cout<<"Given name was not present in the table"<<endl;
	}
      else if(choice=="print")
	{
	  grades->printAll();
	}
      else if(choice=="size")
	{
	  cout<<grades->numEntries()<<endl;
	}
      else if(choice=="stats")
	{
	  grades->hashStats(cout);
	}
      else if(choice=="help")
	{
	  cout<<summary;
      }
      else if(choice=="quit")
	break;
      else
	{
	  cout<<"ERROR: invalid command"<<endl;
	  cout<<summary;
	}
    }
  return 0;
}
