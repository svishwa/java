public class MazeTester {
	public static void main(String[] args) {
		boolean[][] matrix = { { false, false, true }, { true, false, true } };
		MazeCoord mst = new MazeCoord(0, 0);
		MazeCoord ext = new MazeCoord(1, 1);
		Maze check = new Maze(matrix, mst, ext);
		// check.printMatrix();
		System.out.println(check.hasWallAt(mst));
		MazeCoord m1 = check.getExitLoc();
		System.out.println(m1.getRow() + m1.getCol());
		System.out.println(check.numCols());
	}
}
