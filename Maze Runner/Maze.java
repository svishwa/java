// Name:Sandesh Vishwanath
// USC loginid:sandeshv
// CS 455 PA3
// Fall 2017

import java.util.LinkedList;

/**
 * Maze class
 * 
 * Stores information about a maze and can find a path through the maze (if
 * there is one).
 * 
 * Assumptions about structure of the maze, as given in mazeData, startLoc, and
 * endLoc (parameters to constructor), and the path: -- no outer walls given in
 * mazeData -- search assumes there is a virtual border around the maze (i.e.,
 * the maze path can't go outside of the maze boundaries) -- start location for
 * a path is maze coordinate startLoc -- exit location is maze coordinate
 * exitLoc -- mazeData input is a 2D array of booleans, where true means there
 * is a wall at that location, and false means there isn't (see public FREE /
 * WALL constants below) -- in mazeData the first index indicates the row. e.g.,
 * mazeData[row][col] -- only travel in 4 compass directions (no diagonal paths)
 * -- can't travel through walls
 */

public class Maze {

	public static final boolean FREE = false;
	public static final boolean WALL = true;
	public boolean[][] mazeMatrix;
	private int entryRow, entryCol, exitRow, exitCol;
	public static LinkedList<MazeCoord> pathList = new LinkedList<MazeCoord>();
	private boolean searchCheck = false;

	/**
	 * Constructs a maze.
	 * 
	 * @param mazeData
	 *            the maze to search. See general Maze comments above for what
	 *            goes in this array.
	 * @param startLoc
	 *            the location in maze to start the search (not necessarily on
	 *            an edge)
	 * @param exitLoc
	 *            the "exit" location of the maze (not necessarily on an edge)
	 *            PRE: 0 <= startLoc.getRow() < mazeData.length and 0 <=
	 *            startLoc.getCol() < mazeData[0].length and 0 <=
	 *            endLoc.getRow() < mazeData.length and 0 <= endLoc.getCol() <
	 *            mazeData[0].length
	 */
	public Maze(boolean[][] mazeData, MazeCoord startLoc, MazeCoord exitLoc) {
		mazeMatrix = new boolean[mazeData.length][mazeData[0].length];
		for (int i = 0; i < mazeData.length; i++) {
			for (int j = 0; j < mazeData[0].length; j++) {
				if (mazeData[i][j] == FREE)
					mazeMatrix[i][j] = FREE;
				else {
					mazeMatrix[i][j] = WALL;
				}
			}
		}
		entryRow = startLoc.getRow();// to get entry location row number
		entryCol = startLoc.getCol();// to get entry location column number
		exitRow = exitLoc.getRow();// to get exit location row number
		exitCol = exitLoc.getCol();// to get exit location column number
	}

	/**
	 * Returns the number of rows in the maze
	 * 
	 * @return number of rows
	 */
	public int numRows() {
		return mazeMatrix.length; // DUMMY CODE TO GET IT TO COMPILE
	}

	/**
	 * Returns the number of columns in the maze
	 * 
	 * @return number of columns
	 */
	public int numCols() {
		return mazeMatrix[0].length; // DUMMY CODE TO GET IT TO COMPILE
	}

	// used for testing
	/*
	 * public void printMatrix() { for(int i=0;i<numRows();i++) { for(int
	 * j=0;j<numCols();j++) { System.out.print(mazeMatrix[i][j]+" "); }
	 * System.out.println(); } }
	 */

	/**
	 * Returns true iff there is a wall at this location
	 * 
	 * @param loc
	 *            the location in maze coordinates
	 * @return whether there is a wall here PRE: 0 <= loc.getRow() < numRows()
	 *         and 0 <= loc.getCol() < numCols()
	 */
	public boolean hasWallAt(MazeCoord loc) {
		if (mazeMatrix[loc.getRow()][loc.getCol()] == WALL)
			return true;
		return false;
	}

	/**
	 * Returns the entry location of this maze.
	 */
	public MazeCoord getEntryLoc() {
		return new MazeCoord(entryRow, entryCol);
	}

	/**
	 * Returns the exit location of this maze.
	 */
	public MazeCoord getExitLoc() {
		return new MazeCoord(exitRow, exitCol);
	}

	/**
	 * Returns the path through the maze. First element is start location, and
	 * last element is exit location. If there was not path, or if this is
	 * called before a call to search, returns empty list.
	 * 
	 * @return the maze path
	 */
	public LinkedList<MazeCoord> getPath() {
		return pathList;

	}

	/**
	 * Find a path from start location to the exit location (see Maze
	 * constructor parameters, startLoc and exitLoc) if there is one. Client can
	 * access the path found via getPath method.
	 * 
	 * @return whether a path was found.
	 */
	public boolean search() {

		int[][] visited = new int[numRows()][numCols()];// 2D array of
														// mazeMatrix size to
														// keep track of visited
														// cells
		for (int i = 0; i < numRows(); i++) {
			for (int j = 0; j < numCols(); j++) {
				visited[i][j] = 0;// Initializing to zero indication that the
									// cells are free
			}
		}
		if (searchCheck != true)// prevents appending of the same path to the
								// linkedList if the path has already been found
		{
			searchCheck = searchHelper(mazeMatrix, visited, entryRow, entryCol,
					exitRow, exitCol);
		}
		return searchCheck;

	}

	/**
	 * 
	 * @param maze
	 *            copy of instance variable mazeMatrix
	 * @param visited
	 *            2D array to keep track of matrix cells visited
	 * @param x
	 *            start location row number
	 * @param y
	 *            start location column number
	 * @param a
	 *            exit location row number
	 * @param b
	 *            exit location column number
	 * @return returns true if there is a path in the matrix to reach exit
	 *         location
	 * PRE:x>=0 and x<rowSize of maze matrix      
	 * 	   y>=0 and y<colSize of maze matrix
	 */
	private boolean searchHelper(boolean[][] maze, int[][] visited, int x,
			int y, int a, int b) {

		if (maze[x][y] == WALL) {
			return false;
		}// checks if the current location is wall
		if (visited[x][y] == 1) {
			return false;
		}
		if (x == a && y == b)// checks if the location is exit
		{
			visited[x][y] = 1;
			pathList.addFirst(new MazeCoord(x, y));
			return true;
		}
		if (x >= 0 && x < numRows() && y >= 0 && y < numCols()
				&& visited[x][y] == 0 && maze[x][y] == false) {
			visited[x][y] = 1;

			if (x + 1 < numRows())// this condition prevents array index out of
									// bound exception
			{
				if (searchHelper(maze, visited, x + 1, y, a, b))// right
																// direction
				{
					pathList.addFirst(new MazeCoord(x, y));// adds the valid
															// node to
															// linkedList
					return true;
				}
			}
			if (y + 1 < numCols())// this condition prevents array index out of
									// bound exception
			{
				if (searchHelper(maze, visited, x, y + 1, a, b))// up direction
				{
					pathList.addFirst(new MazeCoord(x, y));
					return true;
				}
			}
			if (x - 1 >= 0)// this condition prevents array index out of bound
							// exception
			{
				if (searchHelper(maze, visited, x - 1, y, a, b))// left
																// direction
				{
					pathList.addFirst(new MazeCoord(x, y));
					return true;
				}
			}
			if (y - 1 >= 0)// this condition prevents array index out of bound
							// exception
			{
				if (searchHelper(maze, visited, x, y - 1, a, b))// down
																// direction
				{
					pathList.addFirst(new MazeCoord(x, y));
					return true;
				}
			}
			return false;

		}

		return false;

	}

}