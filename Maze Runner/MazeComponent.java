// Name:Sandesh Vishwanath
// USC loginid:sandeshv
// CS 455 PA3
// Fall 2017

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Graphics2D;
import javax.swing.JComponent;
import java.awt.geom.Line2D;
import java.util.LinkedList;

/**
 * MazeComponent class
 * 
 * A component that displays the maze and path through it if one has been found.
 */
public class MazeComponent extends JComponent {

	private static final int START_X = 10; // top left of corner of maze in
											// frame
	private static final int START_Y = 10;
	private static final int BOX_WIDTH = 20; // width and height of one maze
												// "location"
	private static final int BOX_HEIGHT = 20;
	private static final int INSET = 2;
	// how much smaller on each side to make entry/exit inner box
	private static final int OFFSET = 10;// to move the path from the middle of
											// each road,to offset x axis value
	private Maze maze;
	private String[][] pathLoc = new String[20][20];// to store location of all
													// elements in the matrix

	/**
	 * Constructs the component.
	 * 
	 * @param maze
	 *            the maze to display
	 */
	public MazeComponent(Maze maze) {
		this.maze = maze;
	}

	/**
	 * Draws the current state of maze including the path through it if one has
	 * been found.
	 * 
	 * @param g
	 *            the graphics context
	 */
	public void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		Rectangle bigBox = new Rectangle(START_X, START_Y, BOX_WIDTH
				* maze.numCols(), BOX_HEIGHT * maze.numRows());// Creates a big
																// box of matrix
																// size
		g2.setColor(Color.BLACK);
		g2.draw(bigBox);
		Rectangle box = new Rectangle(START_X, START_Y, BOX_WIDTH, BOX_HEIGHT);// creates
																				// small
																				// boxes
																				// to
																				// represent
																				// each
																				// element
																				// in
																				// a
																				// matrix
		for (int i = 0; i < maze.numRows(); i++) {
			for (int j = 0; j < maze.numCols(); j++) {
				pathLoc[i][j] = box.getX() + "@" + box.getY();
				MazeCoord mzcrd = new MazeCoord(i, j);
				if (maze.mazeMatrix[i][j] == Maze.WALL)// checks for element
														// with true(wall) in
														// its location
				{
					g2.setColor(Color.BLACK);// will be set with black color
					g2.fill(box);
				} else // checks for element with false(free) in its location
				{ 
					g2.setColor(Color.WHITE);// will be set with white color
					g2.fill(box);
				}
				if (mzcrd.equals(maze.getEntryLoc()))// checks for starting
														// point
				{
					Rectangle startBox = new Rectangle(
							(int) box.getX() + INSET, (int) box.getY() + INSET,
							BOX_WIDTH - INSET * INSET, BOX_HEIGHT - INSET
									* INSET);// reduces the size of starting
												// element
					g2.setColor(Color.YELLOW);// will be set with yellow color
					g2.fill(startBox);
				}
				if (mzcrd.equals(maze.getExitLoc())) // checks for exit location
				{
					Rectangle exitBox = new Rectangle((int) box.getX() + INSET,
							(int) box.getY() + INSET,
							BOX_WIDTH - INSET * INSET, BOX_HEIGHT - INSET
									* INSET);// reduces size of exit location
					g2.setColor(Color.GREEN);// will be set with green color
					g2.fill(exitBox);
				}

				box.translate(BOX_WIDTH, 0);// moves to the next column element
			}
			box.translate(-BOX_WIDTH * maze.numCols(), BOX_HEIGHT);// moves to
																	// the next
																	// row

		}
		LinkedList<MazeCoord> mazeList = maze.getPath();// has linkedList of
														// nodes representing
														// path
		for (int i = 0; i < mazeList.size(); i++) {
			if (i + 1 < mazeList.size()) {
				MazeCoord currNode = mazeList.get(i);// gets the node for the
														// current index
				MazeCoord nextNode = mazeList.get(i + 1);// gets the next node
				String currLoc = pathLoc[currNode.getRow()][currNode.getCol()];// gets
																				// the
																				// location
																				// for
																				// current
																				// node
				String[] currCoord = currLoc.split("@", 2);// splits it to get x
															// and y coordinates
				int x1 = (int) Double.parseDouble(currCoord[0]);// x coordinate
																// for current
																// location
				int y1 = (int) Double.parseDouble(currCoord[1]);// y coordinate
																// for current
																// location
				String nextLoc = pathLoc[nextNode.getRow()][nextNode.getCol()];// gets
																				// the
																				// location
																				// for
																				// next
																				// node
				String[] nextCoord = nextLoc.split("@", 2);// splits it to get x
															// and y coordinates
				int x2 = (int) Double.parseDouble(nextCoord[0]);// x coordinate
																// for next
																// location
				int y2 = (int) Double.parseDouble(nextCoord[1]);// y coordinate
																// for next
																// location
				Line2D.Double segment = new Line2D.Double(x1 + OFFSET, y1
						+ OFFSET, x2 + OFFSET, y2 + OFFSET);// line segment from
															// current location
															// to next location
				g2.setColor(Color.BLUE);
				g2.draw(segment);
			}
		}
	}

}
